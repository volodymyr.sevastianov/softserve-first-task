import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { Observable } from 'rxjs';
import { AngularFireDatabase } from 'angularfire2/database';
import { map } from "rxjs/operators";
import { Item } from './models/item';

@Injectable({
    providedIn: 'root'
})
export class PostService {
    database = firebase.database();
    posts: Observable<any[]>;
    postData = new Item();
    path = '/Posts/';

    constructor(public db: AngularFireDatabase) {
    }

    setPost(name, description, category, date, deleted, draft, newPostKey = this.database.ref().child('Posts').push().key) {
        // var newPostKey = this.database.ref().child('Posts').push().key;
        // this.postData = new Item(name, description, category, date, deleted, draft);
        this.postData.name = name;
        this.postData.description = description;
        this.postData.category = category;
        this.postData.expiringDate = date;
        this.postData.uid = "ben";
        this.postData.deleted = deleted;
        this.postData.draft = draft;
        var updates = {};
        console.log(newPostKey);

        updates[this.path + newPostKey] = this.postData;

        return firebase.database().ref().update(updates);
    }

    getPublishedPosts(postsQuantity, key) {
        let query;
        return this.posts = this.db.list(this.path, ref => {
            query = (key) ?
                ref.orderByKey().endAt(key).limitToLast(postsQuantity) :
                ref.orderByKey().limitToLast(postsQuantity);
            return query;
            // return ref.orderByChild('deleted').equalTo(false)
        }).snapshotChanges().pipe(
            map(changes => {
                return changes.map(c => ({
                    key: c.payload.key, ...c.payload.val()
                }))
            }))
    }

    getAllPosts() {

        return this.posts = this.db.list(this.path).snapshotChanges().pipe(
            map(changes => {
                return changes.map(c => ({
                    key: c.payload.key, ...c.payload.val()
                }))
            }))
    }

    deletePost(post) {
        var updates = {};
        this.postData.name = post.name;
        this.postData.description = post.description;
        this.postData.category = post.category;
        this.postData.expiringDate = post.expiringDate;
        this.postData.uid = "ben";
        this.postData.deleted = true;
        this.postData.draft = false;
        updates[this.path + post.key] = this.postData;
        return firebase.database().ref().update(updates);
    }
}
