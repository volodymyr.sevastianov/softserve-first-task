import { PostService } from './../post.service';
import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-separate-post',
    templateUrl: './separate-post.component.html',
    styleUrls: ['./separate-post.component.css']
})
export class SeparatePostComponent implements OnInit {
    highlight = false;
    @Input("post") post;
    @Input("name") name;
    @Input("description") description;
    @Input("category") category;
    @Input("expiringDate") expiringDate;
    constructor(private db: PostService) { }

    ngOnInit() {
    }

    highLight1(){
        this.highlight = true;
    }

    highLight2(){
        this.highlight = false;
    }
    deletePost(post){
        this.db.deletePost(post);
    }

}
