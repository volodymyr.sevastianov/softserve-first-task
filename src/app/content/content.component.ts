import { PostService } from './../post.service';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-content',
    templateUrl: './content.component.html',
    styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {
    postObj;
    endKey;
    constructor(public db: PostService) { }

    ngOnInit() {
        // this.db.getPublishedPosts().subscribe(items => {
        //     this.postObj = items;
        //     this.endKey = this.postObj.last().key;
        //     console.log(this.endKey);
            
        // });
        this.loadPosts();
    }

    loadPosts() {


        this.db.getPublishedPosts(5, this.endKey)
          .subscribe(post => {
            console.log(post);
     
            if (this.postObj) {
              for (let i = post.length - 2; i >= 0; i--) {
                this.postObj.push(post[i]);
              }
            }
            else
              this.postObj = post.reverse();
            this.endKey = this.postObj[this.postObj.length-1].key;
            // this.showSpinner = false;
          });
     
     
      }
    onScroll(){
        this.loadPosts();
        console.log('Laletskiy, ono rabotaet!');
    }

}
