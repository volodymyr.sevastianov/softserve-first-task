import { PostService } from './post.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HeaderComponent } from './header/header.component';
import { ContentComponent } from './content/content.component';
import { FiltersComponent } from './filters/filters.component';
import { PostTableComponent } from './post-table/post-table.component';
import { PostCreatingComponent } from './post-creating/post-creating.component';
import { LogInComponent } from './login/log-in.component';
import { HomeComponent } from './home/home.component';
import { UserComponent } from './user/user.component';
import { LoginpageComponent } from './loginpage/loginpage.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { environment } from 'src/environments/environment.prod';
import { SeparatePostComponent } from './separate-post/separate-post.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { Categories } from './categories.enum';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';


@NgModule({
    declarations: [
        AppComponent,
        NavbarComponent,
        HeaderComponent,
        ContentComponent,
        FiltersComponent,
        PostTableComponent,
        PostCreatingComponent,
        LogInComponent,
        HomeComponent,
        UserComponent,
        LoginpageComponent,
        SeparatePostComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        NgbModule,
        ReactiveFormsModule,
        FormsModule,
        AngularFireDatabaseModule,
        AngularFireAuthModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFontAwesomeModule,
        InfiniteScrollModule
    ],
    entryComponents:[
        PostCreatingComponent,
    ]
    ,
    providers: [
        PostService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
