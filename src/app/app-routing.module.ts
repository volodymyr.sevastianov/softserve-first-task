import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginpageComponent } from './loginpage/loginpage.component';
import { UserComponent } from './user/user.component';

const routes: Routes = [
    {path:'home', component: HomeComponent},
    {path:'login', component: LoginpageComponent},
    {path:'user', component: UserComponent},
    {path: '',   redirectTo: '/home', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
