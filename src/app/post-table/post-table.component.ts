import { PostService } from './../post.service';
import { Component, OnInit, ViewChild } from '@angular/core';


@Component({
    selector: 'app-post-table',
    templateUrl: './post-table.component.html',
    styleUrls: ['./post-table.component.css'],
})
export class PostTableComponent implements OnInit {
    // @ViewChild(PostCreatingComponent) child:PostCreatingComponent;
    postObj;
    buttonEditClass = 'edit-button btn btn-outline-secondary fas fa-pen-square';
    buttonDefaultClass = 'btn btn-success';
    buttonEditTitle = null;
    buttonDefaultTitle = 'Create Post';
    constructor(public db: PostService) { }

    ngOnInit() {
        this.db.getAllPosts().subscribe(items => {
            this.postObj = items;
        });
    }
    
    editPost(post){
        this.db.setPost(post.name, post.description, post.category, post.expiringDate, post.deleted, post.draft, post.key);
    }

}