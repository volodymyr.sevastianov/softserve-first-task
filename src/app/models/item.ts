import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

export class Item{
    constructor(
        public name?: string,
        public description?: string,
        public category?: string,
        public uid?: string,
        public expiringDate?: NgbDateStruct,
        public deleted?: boolean,
        public draft?: boolean
        ){
            draft = false;
            deleted = false;
        }
}