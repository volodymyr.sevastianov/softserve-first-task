import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostCreatingComponent } from './post-creating.component';

describe('PostCreatingComponent', () => {
  let component: PostCreatingComponent;
  let fixture: ComponentFixture<PostCreatingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostCreatingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostCreatingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
