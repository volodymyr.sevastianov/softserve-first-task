import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { PostService } from "../post.service";
import { Item } from '../models/item';
import { Categories } from "../categories.enum";

@Component({
    selector: 'app-post-creating',
    templateUrl: './post-creating.component.html',
    styleUrls: ['./post-creating.component.css']
})
export class PostCreatingComponent implements OnInit {
    @Input() buttonClass;
    @Input() buttonTitle;
    @Input() editPostData;

    closeResult: string;
    postData = new Item();
    tempPostData

    editMode = false;

    categoriesList: string[];
    Categories: typeof Categories = Categories;

    undefinedObj: any;
    modalTitle: string;

    currentDay = new Date().getDate() + 1;
    currentMonth = new Date().getMonth() + 1;
    currentYear = new Date().getFullYear();
    minDate: NgbDateStruct;

    constructor(private modalService: NgbModal, public post: PostService) {
        this.minDate = { year: this.currentYear, month: this.currentMonth, day: this.currentDay };
        if (this.buttonTitle == 'Create Post')
            this.modalTitle = 'Create Post';
        else this.modalTitle = 'Edit Post';
    }

    open(content) {
        this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (!this.editMode) {
            this.postData = { name: null, description: null, category: this.undefinedObj, expiringDate: null };
        }

        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    onSubmit() {
        this.postData.deleted = false;
        this.postData.draft = false;
        if (this.postData != null && this.editPostData != null) {
            this.post.setPost(this.postData.name, this.postData.description, this.postData.category, this.postData.expiringDate, this.postData.deleted, this.postData.draft, this.editPostData.key);
        } else {

        }
        this.postData = { name: null, description: null, category: this.undefinedObj, expiringDate: null };
    }

    onSaveClick() {
        this.postData.deleted = false;
        this.postData.draft = true;
        if (this.postData != null && this.editPostData != null) {
            this.post.setPost(this.postData.name, this.postData.description, this.postData.category, this.postData.expiringDate, this.postData.deleted, this.postData.draft, this.editPostData.key);
        } else {
            this.post.setPost(this.postData.name, this.postData.description, this.postData.category, this.postData.expiringDate, this.postData.deleted, this.postData.draft);

        }
    }

    ngOnInit() {
        if (this.editPostData) {
            this.postData = {...this.editPostData};
            this.editMode = true;
        } else this.editMode = false;
        this.categoriesList = Object.keys(Categories);
        this.categoriesList = this.categoriesList.slice(this.categoriesList.length / 2)
    }

}
